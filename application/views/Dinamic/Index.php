<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?= base_url()?>assets/librerias/Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">
</head>
<body>
	<?=form_open(base_url().'Login',['method'=>'post','class'=>'form'])?>
	<div class="formulario">
		<div class="form-group">
			<label for="email">Email</label>
			<?= form_input(['type'=>'email','name'=>'email','class'=>'form-control','id'=>'email']) ?>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<?= form_input(['type'=>'password','name'=>'password','class'=>'form-control','id'=>'password']) ?>
		</div>
		<div class="form-group">
			<?= form_button(['content'=>'Enviar','type'=>'submit','class'=>'btn btn-success']) ?>
		</div>
	</div>
	<?=form_close()?>
	<script src="<?=base_url()?>assets/librerias/Jquery/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/Bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/script.js"></script>
</body>
</html>